#-------------------------------------------------
#
# Project created by QtCreator 2013-10-22T02:21:59
#
#-------------------------------------------------

QT       += core \
            sql

QT       -= gui

TARGET = ForumStats
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += alphanumtrie.hpp
