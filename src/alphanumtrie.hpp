#ifndef ALPHANUMTRIE_HPP_INCLUDED
#define ALPHANUMTRIE_HPP_INCLUDED

#include <memory>
#include <stdexcept>
#include <cctype>
#include <string>
#include <cstring>
#include <cassert>
#include <algorithm>

// Trie node for alphanumeric characters [0-9a-z]
class AlphaNumTrie
{
    AlphaNumTrie *parent;
    char letter;
    // Pointers for the next trie nodes
    std::unique_ptr<AlphaNumTrie> next[36];
    // >0 if the current node marks the end of a word; counts the times it
    // has been added
    unsigned int finalCount;

    // Gets the letter-index (range [0, 35]) of the first valid letter
    // in the word; moves the pointer to skip all invalid characters
    // Returns -1 if the word ended before finding any good character.
    int GetFirstIdxMovePtr(const char * & word) const
    {
        char cur=*word;
        // Skip the garbage
        while(cur && !std::isalnum(cur))
        {
            cur=0;
            // Fix italian lowercase accented letters (works for UTF-8 input)
            if((unsigned char)*word==0xc3)
            {
                switch((unsigned char)word[1])
                {
                case 0xa0:
                case 0xa1:
                    cur='a';
                    break;
                case 0xa8:
                case 0xa9:
                    cur='e';
                    break;
                case 0xac:
                case 0xad:
                    cur='i';
                    break;
                case 0xb2:
                case 0xb3:
                    cur='o';
                    break;
                case 0xb9:
                case 0xc0:
                    cur='u';
                    break;
                }
            }
            word++;
            if(cur==0)
                cur=*word;
        }
        // If we reached the end of the word, return a negative value
        if(!cur)
            return -1;
        // Calculate the index
        int idx;
        if(cur>='0' && cur<='9')
            idx=cur-'0';
        else
        {
            idx=std::tolower(cur)-'a'+10;
            if(idx<10 || idx>=36)
                return -1;
        }
        return idx;
    }

    // Disable copy construction (already killed by unique_ptr, but who knows)
    AlphaNumTrie(const AlphaNumTrie &)=delete;

public:

    // Construct a new node; by default it's a non-terminal one.
    AlphaNumTrie(AlphaNumTrie * Parent=nullptr, char Letter=0)
        : parent(Parent), letter(Letter), finalCount(false)
    {}

    // True if the node matches the end of a word
    unsigned int GetFinalCount() const
    {
        return finalCount;
    }

    AlphaNumTrie * GetParent() { return parent; }
    const AlphaNumTrie * GetParent() const { return parent; }

    void GetReversedFullWord(std::string &Word) const
    {
        if(parent==nullptr)
            return;
        Word+=letter;
        parent->GetReversedFullWord(Word);
    }

    std::string GetFullWord() const
    {
        std::string word;
        GetReversedFullWord(word);
        std::reverse(word.begin(), word.end());
        return word;
    }

    // Gets the next trie node from the given char
    // (mainly as interface from the outside)
    /*const AlphaNumTrie * GetNextTrie(char ch) const
    {
        int idx=std::tolower(ch)-'a';
        if(idx<0 || idx>=26)
            throw std::invalid_argument("Invalid character");
        return next[idx].get();
    }*/

    const AlphaNumTrie * Next(int id) const
    {
        return next[id].get();
    }

    // Adds a word to the trie
    AlphaNumTrie *AddWord(const char * word)
    {
        // Get the first character-index of word
        int idx=GetFirstIdxMovePtr(word);
        if(idx<0)
        {
            // If the word ended, then the current node is finalCount
            finalCount++;
            return this;
        }
        // If there's no child node for it yet, create it
        if(next[idx].get()==NULL)
        {
            char c;
            if(idx<10)
                c='0'+idx;
            else
                c='a'+(idx-10);
            next[idx].reset(new AlphaNumTrie(this, c));
        }
        // Recurse for the next letter
        return next[idx]->AddWord(word+1);
    }

    // Check if the given word is present
    bool IsWordPresent(const char * word) const
    {
        // Get the terminal trie treating word as a prefix
        const AlphaNumTrie * pt=GetPrefixTrie(word);
        // Check if it actually exists and is finalCount
        return pt!=NULL && pt->finalCount;
    }

    // Find the terminal trie node for the given prefix (if present)
    const AlphaNumTrie * GetPrefixTrie(const char * prefix) const
    {
        int idx=GetFirstIdxMovePtr(prefix);
        // If the word has ended, this *is* the terminal trie
        if(idx<0)
            return this;
        // No node for the current letter => the prefix is unknown
        if(next[idx].get()==NULL)
            return NULL;
        // Recurse on the next letter
        return next[idx]->GetPrefixTrie(prefix+1);
    }

    // Count the nodes present under this trie node
    unsigned int CountNodes()
    {
        // This is one node...
        unsigned int ret=1;
        // ... then recurse!
        for(unsigned int i=0; i<36; i++)
            if(next[i].get()!=NULL)
                ret+=next[i]->CountNodes();
        return ret;
    }

    // Resets the node
    void reset()
    {
        for(unsigned int i=0; i<36; i++)
            next[i].reset();
        finalCount=0;
    }
};

#endif
