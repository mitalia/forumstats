#include <QCoreApplication>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "alphanumtrie.hpp"
#include <QVariant>
#include <string>
#include <iostream>
#include <queue>
#include <algorithm>
#include <QStringList>

#include <unordered_map>

#define USE_HASHMAP

// Trick to get the underlying container of an adaptor container
template <class Container>
class Adapter : public Container {
public:
    typedef typename Container::container_type container_type;
    container_type &get_container() { return this->c; }
};

// Functor used to compare AlphaNumTries using the add-count as compare key
struct greater_at
{
    bool operator()(const AlphaNumTrie * l, const AlphaNumTrie * r)
    {
        return l->GetFinalCount()>r->GetFinalCount();
    }
};

typedef Adapter<std::priority_queue<const AlphaNumTrie *, std::deque<const AlphaNumTrie *>, greater_at>> wpq;

// Fills the Word Priority Queue using the given trie; called recursively
void fillWPQ(const AlphaNumTrie & Trie, wpq &WPQ, unsigned int MaxWPQSize);


void fillWPQ(const AlphaNumTrie & Trie, wpq &WPQ, unsigned int MaxWPQSize)
{
    if(Trie.GetFinalCount()!=0)
    {
        // We got an end-trie of an actual word
        // If the priority queue isn't full yet, just add it
        if(WPQ.size()<MaxWPQSize)
            WPQ.push(&Trie);
        // Otherwise, check if it's better than our worst word
        else if(Trie.GetFinalCount()>=WPQ.top()->GetFinalCount())
        {
            // In that case, kill the worst word and add this one
            WPQ.pop();
            WPQ.push(&Trie);
        }
    }
    // Check the children tries
    for(int i=0; i<36; ++i)
    {
        if(Trie.Next(i)!=nullptr)
            fillWPQ(*Trie.Next(i), WPQ, MaxWPQSize);
    }
}

int WordFreq(QSqlDatabase &db)
{
    // Get all the posts
    QSqlQuery q("select content, date from posts", db);
    q.setForwardOnly(true);
    if(!q.exec())
    {
        std::cerr<<"Query failed\n";
        return 3;
    }

#ifdef USE_HASHMAP
    std::unordered_map<std::string, unsigned int> um;
#else
    // Trie root
    AlphaNumTrie at;
#endif
    // Current word
    QString word;
    word.reserve(256);
    // Records count
    int c=0;
    while(q.next())
    {
        c++;
        // Diagnostics (just so that we know the program isn't stuck)
        if(c%5000==0)
            std::cerr<<c<<"\r";
        // Get the post content, with accents & co. split in separate characters (Unicode KD normalization)
        QString str=q.value(0).toString().normalized(QString::NormalizationForm_KD);
        // true if we are currently in a word (=consecutive sequence of alphanumeric characters)
        bool inWord=false;
        for(QChar &c: str)
        {
            // Append characters to the current word if they are good
            if(c.isLetterOrNumber())
            {
                word.append(c);
                inWord=true;
            }
            else
            {
                // At the first bad character, the current word ends
                if(inWord)
                {
                    // Add it to the trie
#ifdef USE_HASHMAP
                    um[word.toStdString()]++;
#else
                    if(at.AddWord(word.toUtf8())==&at)
                        std::cerr<<"\nCompletely rejected word: "<<word.toStdString()<<"\n";
#endif
                    // New word
                    word.clear();
                    inWord=false;
                }
            }
        }
    }
#ifdef USE_HASHMAP
    struct umitcomparer
    {
        typedef typename std::unordered_map<std::string, unsigned int>::iterator itt;

        bool operator()(itt l, itt r)
        {
            return l->second>r->second;
        }
    };

    Adapter<std::priority_queue<umitcomparer::itt, std::deque<umitcomparer::itt>,  umitcomparer>> m;
    for(auto it=um.begin(); it!=um.end(); ++it)
    {
        if(m.size()<10000)
            m.push(it);
        else
        {
            if(m.top()->second<it->second)
            {
                m.pop();
                m.push(it);
            }
        }
    }
#else
    // Word Priority Queue; stores trie nodes corresponding to the end of a word, keeping
    // only the best ones; this allows for quick comparisons and small memory footprint
    wpq m;
    // Fill the WPQ
    fillWPQ(at, m, 10000);
#endif

    // Get the underlying container
    auto &d=m.get_container();
#ifdef USE_HASHMAP
    std::sort_heap(d.begin(), d.end(), umitcomparer());
    for(const auto &t: d)
    {
        std::cout<<t->first<<": "<<t->second<<"\n";
    }
#else
    // De-heapize it (we get a list sorted from higher to lower count)
    std::sort_heap(d.begin(), d.end(), greater_at());
    // Print the stuff
    for(const AlphaNumTrie *t: d)
        std::cout<<t->GetFullWord()<<": "<<t->GetFinalCount()<<"\n";
#endif
    //return a.exec();
    return 0;
}


int AdjacencyMatrix(QSqlDatabase &db)
{
    std::unordered_map<unsigned int, unsigned int> UID2MatrixID;
    std::vector<unsigned int> threadIDs;
    const unsigned int usersNumber=1000;
    std::vector<std::string> topUserNames;
    topUserNames.reserve(1000);
    std::clog<<"Top users query...\n";
    std::vector<unsigned int> adjacencyMatrix(usersNumber*usersNumber);
    {
        QSqlQuery q("select userid, count(id) as postcount, username from posts where userid<>0 group by userid order by postcount desc limit ?", db);
        q.addBindValue(usersNumber);
        q.setForwardOnly(true);
        if(!q.exec())
        {
            std::cerr<<"Top users query failed\n";
            return 3;
        }
        unsigned int matrixID=0;
        while(q.next())
        {
            unsigned int uid=q.value(0).toUInt();
            UID2MatrixID[uid]=matrixID++;
            topUserNames.push_back(q.value(2).toString().toStdString());
        }
    }
    std::cerr<<topUserNames.size()<<"\n";
    std::clog<<"Threads query...\n";
    {
        QSqlQuery q("select ID from threads", db);
        q.setForwardOnly(true);
        if(!q.exec())
        {
            std::cerr<<"ThreadIDs query failed\n";
            return 4;
        }
        while(q.next())
        {
            threadIDs.push_back(q.value(0).toUInt());
        }
    }
    {
        std::clog<<"Creating index...\n";
        QSqlQuery q("create index if not exists threadids on posts (threadid)");
        if(!q.exec())
        {
            std::cerr<<"Create index query failed\n";
            return 6;
        }
    }
    {
        QSqlQuery q("select userid, count(id) as postcount, threadid from posts where threadID=? and userid<>0 group by userid", db);
        unsigned int c=0;
        for(unsigned int threadID: threadIDs)
        {
            if(c%50)
                std::clog<<"Thread "<<c<<"/"<<threadIDs.size()<<" ("<<c*100/threadIDs.size()<<"%)\r";
            ++c;
            q.bindValue(0, threadID);
            if(!q.exec())
            {
                std::cerr<<"Thread posts count query failed\n";
                return 5;
            }
            std::unordered_map<unsigned int, unsigned int> UID2Points;
            while(q.next())
            {
                UID2Points[q.value(0).toUInt()]=q.value(1).toUInt();
            }
            for(std::pair<const unsigned int, unsigned int> & p1: UID2Points)
            {
                auto uid1=UID2MatrixID.find(p1.first);
                if(uid1==UID2MatrixID.end())
                    continue;
                for(std::pair<const unsigned int, unsigned int> & p2: UID2Points)
                {
                    auto uid2=UID2MatrixID.find(p2.first);
                    if(uid2==UID2MatrixID.end())
                        continue;
                    adjacencyMatrix[uid1->second+usersNumber*uid2->second]+=p1.second+p2.second;
                }
            }
        }
    }
    for(std::string &name: topUserNames)
    {
        std::cout<<"\t"<<name;
    }
    std::cout<<"\n";
    for(unsigned int i=0; i<usersNumber; ++i)
    {
        std::cout<<topUserNames[i];
        for(unsigned int j=0; j<usersNumber; ++j)
        {
            std::cout<<"\t"<<adjacencyMatrix[i*usersNumber+j];
        }
        std::cout<<"\n";
    }
    return 0;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
    if(a.arguments().count()<2)
    {
        std::cerr<<"Not enough arguments (expected database path)\n";
        return 1;
    }
    db.setDatabaseName(a.arguments()[1]);
    if(!db.open())
    {
        std::cerr<<"Couldn't open the DB\n";
        return 2;
    }
    return AdjacencyMatrix(db);
}
